## Introduction:

When it comes to learning Geographical Systems, no one document or single Wiki could encompass the historical and scientific knowledge in the discipline. However, with many more critical individuals, researchers, hackers, scientific communities and groups have gathered, reviewed the collected contents present throughout internet. Learning Geography or GIS or any allied field of interest through strict academic career is not the only way (must not be the only way) to acquire such
great knowledge and contribute back. With that sense, i have tried to collate possible locations of web pages, sites, wikis, tutorials, etc... and encourage who is willing to collage can do the same.

All these disseminated knowledge in internet is the proof of power of commons. It illustrates the desire of anyone who is interested in its science to make it more transparent, open and propagatable to the commons.

### Community Wikis:

| Name			| About					| Links	|
|:----------------------|:--------------------------------------|:------|
| GIS Wiki		|	General GIS			|	http://wiki.gis.com/wiki/index.php/Main_Page 	|
| Landscape Toolbox Wiki | Field & Remote Sensing | http://wiki.landscapetoolbox.org/doku.php/Home |
| Transit Wiki		|	Transit/Transport		|	http://www.transitwiki.org/			|
| TDM			|	Transport Demand & Mgmt		|	http://www.vtpi.org/tdm/tdm12.htm 		|
| Awesome Transit | Community list of transit APIs, apps, datasets, research, and software | https://github.com/CUTR-at-USF/awesome-transit |


### Individual Researcher/Expert Portals/Blogs:

| About			| Name 					| Links	|
|:----------------------|:--------------------------------------|:------|
| Visualization		|	Bernie Jenny			|	http://berniejenny.info/ |
| QGIS Tutorials	|	Ujaval Gandhi			|	http://www.qgistutorials.com/en/ |
| GIS to Remote Sensing	|	Luca Congedo			|	https://fromgistors.blogspot.com/ |
| Karto Pics		|	Daniel Coe			|	https://kartopics.com/ |
| Cartography Workshop	|	Planemad			|	https://en.wikipedia.org/wiki/User:Planemad/Basic_cartography_workshop |
| Satellite Workshop	|	Planemad			|	https://github.com/mapbox/workshops/tree/gh-pages/satellite-workshop |
| Planemad Diary	|	Planemad			|	http://www.openstreetmap.org/user/PlaneMad/diary/ |
| One Metro World	|	Jugcerovic			|	http://www.jugcerovic.com/maps/one-metro-world/ |
| Human Transit		|	Jarret Walker			|	http://humantransit.org/ |
| Finding Graticule	|	xkcd wiki			|	http://wiki.xkcd.com/geohashing/Finding_your_graticule |
| Lets make a Map	|	Mike Bostock			|	https://bost.ocks.org/mike/map/ |
| RStoolbox for R	|	Benjamin Leutner			|   https://bleutner.github.io/RStoolbox/about.html |
| Maps,tools		|	Townsendjennings		|	http://www.townsendjennings.com/maps/ |
| Add isochrones	|	Peter Liu			|	https://blog.mapbox.com/add-isochrones-to-your-next-application-e9e84a62345f |
| Denise Lu		|	Denise Lu			|	https://www.washingtonpost.com/people/denise-lu/?utm_term=.5d0463028b10 |
| Derek Watkins |   Derek Watkins       |   https://www.dwtkns.com/ |
| AC Geospatial |   Andrew Cutts        |   http://www.acgeospatial.co.uk/ |
| Nicolas Brodu |   Nicolas Brodu       |   http://nicolas.brodu.net/en/ |
| Info. Design & Mgmt. | Movable Type    |   https://www.movable-type.co.uk/scripts/latlong.html |


### Community Websites:

| Name			| Links |
|:----------------------|:------|
| OSM Blog		|	https://blog.openstreetmap.org/ |
| Contribution Heat map	|	http://yosmhm.neis-one.org/ |
| Mapschool		|	http://mapschool.io/ ; https://github.com/mapschool/course |
| OpenTopography|   http://www.opentopography.org/ |
| Virtual Terrain | http://vterrain.org/ |
| GeoOps		|	http://geops.ch/?language=en |
| Cartographyts		|	https://sites.google.com/site/cartographyts/ |
| INAT			|	http://www.inat.fr/ |
| Transit Map		|	http://transitmap.net/ |
| DataMeet		|	https://datameet.org/ |
| MIT T-Club		|	http://t-club.mit.edu/ |
| Transfor Map		|	http://transformap.co/ |
| Windy 		|	https://community.windy.com/ |
| Indian Rail Info	|	https://indiarailinfo.com/atlas |

### Institutes:

| Name			| Links	|
|:----------------------|:------|
| UNAVCO		|	https://www.unavco.org/education/education.html |
| ISPRS			|	http://www.isprs.org/default.aspx |
| SIRI			|	http://user47094.vs.easily.co.uk/siri/ |
| WMO			|	http://www.wmo.int/pages/prog/www/IMOP/Knowledge-sharing_Portal.html |
| Oregon State Univ.	|	http://cartography.oregonstate.edu/ |
| Penn State Univ.(PSU)	|	http://www.geog.psu.edu/research/giscience/online-geospatial-education-penn-state |
| PSU E-Education	|	https://www.e-education.psu.edu/geog585/ |
| Online GIS course	|	https://gis.e-education.psu.edu/ |
| Centre for Geospatial Analysis |   https://cnr.ncsu.edu/geospatial/ |
| GeoForAll Lab |   https://geospatial.ncsu.edu/osgeorel/ |
| GeoForAll Lab Repo |   https://github.com/ncsu-geoforall-lab/ |
| PSU + FreeTamilebooks	|	https://ia801508.us.archive.org/1/items/open_web_mapping/open_web_mapping.pdf |
| OpenForis     |   http://www.openforis.org |
| Wshington state Dept. of Natural Resources(WDNR) |   https://www.dnr.wa.gov/ |
| NPTEL         |   https://nptel.ac.in/courses/105107122/
| DST-IGET      |   http://dst-iget.in/about-us |
| Carleton University | http://gracilis.carleton.ca/CUOSGwiki/ |
| IHE Delft | https://ocw.un-ihe.org/ |


### Repositories & Standards:

| Name			| Links |
|:----------------------|:------|
| OGC			|	http://www.opengeospatial.org/ |
| CUGIR			|	 https://cugir.library.cornell.edu/ |
| FGDC			|	 https://www.fgdc.gov/ |
| DCMI			|	 http://dublincore.org/ |
| WDNR GIS Open Data |    http://data-wadnr.opendata.arcgis.com/ |
| GeoMapp		|	 http://www.geomapp.net/ |
| ISO 9241 (ergonomics of human interaction)      |    https://en.wikipedia.org/wiki/ISO_9241 |
| ArthaPedia    |    http://www.arthapedia.in/index.php?title=Home_Page |


### Media Websites:

| Name			| Media				| Links	|
|:----------------------|:--------------------------------------|:------|
| GIS Geography     |   GIS Geography           |   http://gisgeography.com |
| Digital Geography	|	Digital Geography		|	http://www.digital-geography.com/ |
| Geoawesomeness	|	Geoawesomeness			|	https://geoawesomeness.com/ |
| Mobility Lab		|	Mobility Lab			|	https://mobilitylab.org/tech/ |
| All over the Map	|	National Geographic		|	http://www.nationalgeographic.com/people-and-culture/all-over-the-map/ |
| Bare Earth        |   WDNR                    |   https://wadnr.maps.arcgis.com/apps/Cascade/index.html?appid=36b4887370d141fcbb35392f996c82d9 |
| Mapping the Reality | Medium - Mental Maps & GIS |    https://blog.prototypr.io/mapping-the-reality-of-the-world-df7ad81ccb54 |


### Open Data Sources for GIS:

| Name          | Purpose             | Links |
|:----------------------|:--------------------------------------|:------|
| Natural Earth | Physical, Cultural Shapefiles | http://www.naturalearthdata.com/ |
| SRTM          | Terrain Data                  | https://dds.cr.usgs.gov/srtm/version2_1/SRTM3/ |
| AWS           | AWS public dataset            | https://aws.amazon.com/public-datasets/ |
| USGS          | USGS Earth Explorer           | http://earthexplorer.usgs.gov/ |
| Sentinel      | Scientific Data Hub           | https://scihub.copernicus.eu/dhus |
| NOAA          | NOAA CLASS                    | http://www.class.noaa.gov/ |
| NOAA          | NOAA Aeril Photo Ordering     | http://www.ngs.noaa.gov/web/APOS2/APOS.shtml |
| NOAA          | NOAA Data Access Viewer       | https://coast.noaa.gov/dataviewer/# |
| NOAA          | NOAA Digital Coast            | https://coast.noaa.gov/digitalcoast/ |
| NASA          | NASA Reverb                   | http://reverb.echo.nasa.gov/reverb |
| EOLi          | Earth Observation Link        | https://earth.esa.int/web/guest/eoli |
| NISE          | National Ins. Space Research  | http://www.dgi.inpe.br/CDSR/ |
| ISRO          | Bhuvan                        | http://bhuvan.nrsc.gov.in/data/download/index.php |
| JAXA          | ALOS                          | http://www.eorc.jaxa.jp/ALOS/en/aw3d30/ |
| VITO          | Coarse Vegetation Data        | http://www.vito-eodata.be/PDF/portal/Application.html#Home |
| Landcover     | Global Landcover              | http://landcover.org/ |
| UNAVCO        | UNAVCO Research Data          | http://www.unavco.org/ |
| FAO           | Socio-Economic Data           | http://www.fao.org/geonetwork/srv/en/main.home |
| SEDAC         | Socio-Economic Data           | http://sedac.ciesin.columbia.edu/ |
| MIT GeoWeb    | MIT GIS data access           | http://web.mit.edu/geoweb/ |
| STEP          | STEP - ESA                    | http://step.esa.int/main/ |
| PCL Collection | University of Texas Library  | https://legacy.lib.utexas.edu/maps/ |
| TauDEM Test Data |                            | https://github.com/dtarb/TauDEM-Test-Data |
