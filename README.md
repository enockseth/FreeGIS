## Introduction:

Geography is fundamental to evolution. It is also fundamental and absolute to many stuff that we live with in our daily life. Knowing about it reveals its interactions and interconnections between different fields of knowledge. It is so much fortunate that such a versatile knowledge were mostly spear headed with the commitment to openness and anti-enclosure by many enthusiastic geographers, surveyors, cartographers, designers, artists, engineers, developers, integrators, etc.

Now is the time for commons - where the stage of GIS is reaching its Flexibility along with Affordability, Accessibility, Availability - to whomever can able to get an Internet access. History of Internet itself intertwines with the history of GIS in many way. One can able to see the evolution of GIS in a remarkable way right after Internet participation in a profound way which was never before possible.

Studying Geography is no more arcanne (it never was), and we have to be thankful for all those explorers, surveyors, for their tiresome labour they have dedicatedly expanded in their interest and love for the planet, exploration, understanding its complexity, etc. GIS has become more than interesting. It is one such field where information and communication has its huge influence. Anybody who is interested in drawing a map, story telling with it, in various dimensions such as:

* History
* Economics
* Transport & Logistics
* Geology
* Hydrology
* Geo + Hydro Morphology
* Environmental Conservation
* Biodiversity Conservation
* Ecosystem Monitoring, Conservation - both Coastal & Non-Coastal
* Early Warning Systems
* Energy Generation
* Political Science
* Social Science
* Administration, Governance
* Identification
* and many others...

GIS very recently has seen a rapid expansion with powerful processing power and innovative information technology that could combine artistic design and granular engineering in a balanced way.

With the rise in Communities among people, Commons based Peer Production, Global Collaborative projects like GNU, OSGEO, Wikipedia, OpenStreetMap commons are becoming more and more influential than the Governments and Business Corporates. People dominate through democratic practices enabled through Internet based technological solutions which themselves are transparent. It is common sense, that in addition to spending for training authorized survey engineers, we can make technological systems that democratically attract more mass that seamlessly help them participate in an uncomplicated manner, if not alteast in a fun way.

With more and more widening of commons participation in Production of Tools necessary for Exploring and Explaining Geography and its allied disciplines, knowledge, ideas, and information transfer happens at a rate that was never ever happened in the history of Mapping/Cartography. Particularly Research scholars, Interns, from independent and public domains with strong civic participation and activism has changed the course of Transparency of Geography through practising freedom in Data, Information, Documentation, Tooling, Publishing, Dissemination using various available licenses evolving into open standards which further actually strengthens the fundamental pillars of scientific thought, which was significantly damaged for decades with interference from paywalls, proprietary tools, patented and Intellectual propertied tools and techniques literally leading to enclosure of knowledge, scientific knowledge as we know it. 

With new set of platform that ensures scientific principles at its core, new models of economic generation and sustenance were discussed and practiced parallely, thus establishing a non-exploitative, cooperative and collaborative way of making a living while also doing good with Science and derived Technologies.

